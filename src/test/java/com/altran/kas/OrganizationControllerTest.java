package com.altran.kas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.altran.kas.business.dto.OrganizationsResultDTO;

public class OrganizationControllerTest extends AbstractTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void getOrganizations() throws Exception {
		String uri = "/organizations?page=1&size=3";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		OrganizationsResultDTO resultList = super.mapFromJson(content, OrganizationsResultDTO.class);
		assertTrue(resultList.getResults().size() > 0);
	}

}
