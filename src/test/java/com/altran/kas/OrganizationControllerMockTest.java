package com.altran.kas;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.altran.kas.business.dto.OrganizationDTO;
import com.altran.kas.business.dto.OrganizationsResponse;
import com.altran.kas.business.dto.OrganizationsResultDTO;
import com.altran.kas.business.service.OrganizationService;

public class OrganizationControllerMockTest extends AbstractTest {
	
	@MockBean
    private OrganizationService organizationService;

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void getOrganization() throws Exception {
		String organizationCode = "OrganizationCode001";		
		OrganizationsResponse organizationsResponseExpected = this.createOrganizationsResponseToMock(organizationCode);
		when(organizationService.getOrganization(anyString())).thenReturn(organizationsResponseExpected);
		
		String uri = "/organizations/" + organizationCode;
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		String contentExpected = super.mapToJson(organizationsResponseExpected.getContent());
		assertEquals(contentExpected, content);
	}
	
	private OrganizationsResponse createOrganizationsResponseToMock(String organizationCode) {
		OrganizationsResponse organizationsResponseExpected = new OrganizationsResponse();
		OrganizationsResultDTO organizationsResultExpected = new OrganizationsResultDTO();
		OrganizationDTO organizationExpected = new OrganizationDTO(); 
		organizationExpected.setCode(organizationCode);
		organizationsResultExpected.setResults(Arrays.asList(organizationExpected));
		organizationsResponseExpected.setResult(organizationsResultExpected);
		return organizationsResponseExpected;
	}

}
