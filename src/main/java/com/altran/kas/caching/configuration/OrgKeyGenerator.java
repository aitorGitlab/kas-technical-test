package com.altran.kas.caching.configuration;

import java.lang.reflect.Method;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.util.StringUtils;

import com.altran.kas.core.utils.LocaleUtils;

public class OrgKeyGenerator implements KeyGenerator {
	  
    public Object generate(Object target, Method method, Object... params) {
        return target.getClass().getSimpleName() + "_"
          + method.getName() + "_"
          + StringUtils.arrayToDelimitedString(params, "_") 
          + "_" + LocaleUtils.getLocale();
    }

}
