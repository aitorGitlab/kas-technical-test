package com.altran.kas.caching.configuration;

import static com.altran.kas.constants.Constants.KEY_GENERATOR;
import static com.altran.kas.constants.Constants.ORGANIZATION_CACHE;

import java.util.Arrays;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class CachingConfig extends CachingConfigurerSupport {

	@Bean
	@Override
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        Cache booksCache = new ConcurrentMapCache(ORGANIZATION_CACHE);
        cacheManager.setCaches(Arrays.asList(booksCache));
        return cacheManager;
    }
   
    @Bean(KEY_GENERATOR)
    @Override
    public OrgKeyGenerator keyGenerator() {
        return new OrgKeyGenerator();
    }
}
