package com.altran.kas.constants;

public class Constants {
	
	// Http
	public static final String START_PARAM = "start";
	public static final String ROWS_PARAM = "rows";
	public static final String Q_PARAM = "q";
	public static final String LOCALE_PARAM = "locale";

	// Caching
	public static final String ORGANIZATION_CACHE = "organization";
	public static final String KEY_GENERATOR = "keyGenerator";
	
	private Constants() {}
}
