package com.altran.kas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.altran.kas.business.dto.OrganizationsResponse;
import com.altran.kas.business.dto.OrganizationsResultDTO;
import com.altran.kas.business.service.OrganizationService;

@RestController
@RequestMapping(value = {"/organizations", "/{locale}/organizations"})
public class OrganizationController {

	@Autowired
	private OrganizationService organizationService;

	@GetMapping(params = { "page", "size" })
	public ResponseEntity<OrganizationsResultDTO> getOrganizations(
			@RequestParam(value="page") int page, 
			@RequestParam(value="size") int size,
			@PathVariable(value = "locale", required = false) String locale) {

		OrganizationsResponse request = organizationService.getPageFromAllOrganizations(page, size);
		return new ResponseEntity<>(request.getResult(), HttpStatus.OK);
	}
	
	@GetMapping("/{organizationCode}")
	public ResponseEntity<Object> getOrganization(
			@PathVariable("organizationCode") String organizationCode,
			@PathVariable(value = "locale", required = false) String locale) {

		OrganizationsResponse request = organizationService.getOrganization(organizationCode);
		return new ResponseEntity<>(request.getContent(), HttpStatus.OK);
	}

}