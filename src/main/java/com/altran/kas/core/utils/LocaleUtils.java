package com.altran.kas.core.utils;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

import com.altran.kas.core.constants.LocaleConstants;
import com.altran.kas.core.enums.LocaleEnum;

public class LocaleUtils {
	
	/**
	 * Devuelve el locale de Spring en formato texto
	 * @param shortLocale
	 * @return
	 */
	public static String getLocale() {
		return LocaleContextHolder.getLocale().getLanguage();
	}

	/**
	 * 
	 * @param completeLocale
	 */
	public static void setLocale(String completeLocale) {
		
		if(completeLocale == null || completeLocale.length() != 5 || !completeLocale.matches(LocaleConstants.LOCALE_COMPLETE_VALIDATION)) {
			throw new IllegalArgumentException("El locale no tiene el formato correcto");
		}
		
		Locale loc = null;
		
		if(LocaleEnum.existsCompleteLocale(completeLocale)) {
			loc = new Locale(LocaleUtils.getLanguageByCompleteLocale(completeLocale),LocaleUtils.getCountryByCompleteLocale(completeLocale));
		} else {
			loc = new Locale(LocaleUtils.getLanguageByCompleteLocale(LocaleConstants.LOCALE_DEFAULT),LocaleUtils.getCountryByCompleteLocale(LocaleConstants.LOCALE_DEFAULT));
		}
		
		LocaleContextHolder.setLocale(loc);
	}
	
	/**
	 * Devuelve el language de un locale completo.
	 * @param completeLocale Locale con el formato completo. Ejemplo: en_US
	 * @return Language del locale. Ejemplo: en
	 */
	public static String getLanguageByCompleteLocale(String completeLocale) {
		if(completeLocale == null || completeLocale.length() != 5 || !completeLocale.matches(LocaleConstants.LOCALE_COMPLETE_VALIDATION)) {
			throw new IllegalArgumentException("El locale no tiene el formato correcto");
		}
		
		return completeLocale.substring(0, completeLocale.indexOf(LocaleConstants.LOCALE_SEPARATOR));
	}
	
	/**
	 * Devuelve el country de un locale
	 * @param completeLocale Locale con el formato completo. Ejemplo en_US
	 * @return Language del locale. Ejemplo: US
	 */
	public static String getCountryByCompleteLocale(String completeLocale) {
		if(completeLocale == null || completeLocale.length() != 5 || !completeLocale.matches(LocaleConstants.LOCALE_COMPLETE_VALIDATION)) {
			throw new IllegalArgumentException("El locale no tiene el formato correcto");
		}
		
		return completeLocale.substring(completeLocale.indexOf(LocaleConstants.LOCALE_SEPARATOR) + 1, completeLocale.length());
	}

	private LocaleUtils() {
	}
}
