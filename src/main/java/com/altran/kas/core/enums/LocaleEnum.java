package com.altran.kas.core.enums;

import java.util.Arrays;

import org.springframework.util.StringUtils;

public enum LocaleEnum {

	EN("en_US"),
	ES("es_ES"),
	CA("ca_ES");
	
	private String label;

	private LocaleEnum(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	/**
	 * Buscar locale corto en base al nombre del campo
	 * @param locale
	 * @return
	 */
	public static LocaleEnum getShortLocale(String locale) {
		if(StringUtils.isEmpty(locale)) {
			return null;
		}
		return Arrays.stream(LocaleEnum.values()).filter(x -> x.toString().equalsIgnoreCase(locale)).findFirst().orElse(null);
	}
	
	public static boolean existsShortLocale(String locale) {
		return LocaleEnum.getShortLocale(locale) != null;
	}
	
	/**
	 * Buscar locale largo en base al valor del enum
	 * @param completeLocale
	 * @return
	 */
	public static LocaleEnum getCompleteLocale(String completeLocale) {
		return Arrays.stream(LocaleEnum.values()).filter(x -> x.getLabel().equals(completeLocale)).findFirst().orElse(null);
	}
	
	public static boolean existsCompleteLocale(String completeLocale) {
		return LocaleEnum.getCompleteLocale(completeLocale) != null;
	}
}
