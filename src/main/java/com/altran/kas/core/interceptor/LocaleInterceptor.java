package com.altran.kas.core.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;

import com.altran.kas.core.constants.LocaleConstants;
import com.altran.kas.core.enums.LocaleEnum;
import com.altran.kas.core.utils.LocaleUtils;

public class LocaleInterceptor implements HandlerInterceptor {

	private static final Logger logger = LogManager.getLogger(LocaleInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		logger.debug("Request entrante: {}", request.getRequestURL().toString());
		
		final Map<String, String> pathVariables = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		
		if(pathVariables != null) {
			String locale = pathVariables.get("locale");
			LocaleEnum localeEnum = StringUtils.isEmpty(locale) ? null : LocaleEnum.getShortLocale(locale);
			String localeComplete = localeEnum == null ? LocaleConstants.LOCALE_DEFAULT : localeEnum.getLabel();
			LocaleUtils.setLocale(localeComplete);
		}
		return true;
	}
}