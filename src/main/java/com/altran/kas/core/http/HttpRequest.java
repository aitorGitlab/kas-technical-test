package com.altran.kas.core.http;

import java.util.Map;

public interface HttpRequest {
	
	<T> T doGet(String url, Class<T> responseType);
	<T> T doGet(String url, Class<T> responseType, Object... uriVariables);
	<T> T doGet(String url, Map<String,String> paramsHeader, Class<T> responseType);	
	<T> T doGet(String url, Map<String,String> paramsHeader, Class<T> responseType, Object... uriVariables);

}
