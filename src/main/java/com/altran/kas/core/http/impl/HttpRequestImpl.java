package com.altran.kas.core.http.impl;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.altran.kas.core.http.HttpRequest;

@Service
public class HttpRequestImpl implements HttpRequest {

	@Override
	public <T> T doGet(String url, Class<T> responseType) {
		return this.doHttp(url, null, responseType, HttpMethod.GET);
	}

	@Override
	public <T> T doGet(String url, Class<T> responseType, Object... uriVariables) {
		return this.doHttp(url, null, responseType, HttpMethod.GET, uriVariables);
	}

	@Override
	public <T> T doGet(String url, Map<String, String> paramsHeader, Class<T> responseType) {
		return this.doHttp(url, paramsHeader, responseType, HttpMethod.GET);
	}
	
	@Override
	public <T> T doGet(String url, Map<String, String> paramsHeader, Class<T> responseType, Object... uriVariables) {
		return this.doHttp(url, paramsHeader, responseType, HttpMethod.GET, uriVariables);
	}
	
	@SuppressWarnings("unchecked")
	private <T> T doHttp(String url, Map<String, String> paramsHeader, Class<?> responseType, HttpMethod method, Object... uriVariables) {
		RestTemplate restTemplate = new RestTemplate();
				
		HttpEntity<String> entity = this.buildHttpHeaders(paramsHeader);
		
		// Http request
		ResponseEntity<?> result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		try {
			result = restTemplate.exchange(url, method, entity, responseType, uriVariables);
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		
		return (T) result.getBody();
	}
	
	private HttpEntity<String> buildHttpHeaders(Map<String, String> paramsHeader) {
		
		HttpHeaders headers = new HttpHeaders();
		
		if (paramsHeader != null &&  !paramsHeader.isEmpty()) {
			for (Map.Entry<String, String> entry : paramsHeader.entrySet()) {
				headers.add(entry.getKey(), entry.getValue());
			}
		}
		
		return new HttpEntity<>(headers);
	}

}
