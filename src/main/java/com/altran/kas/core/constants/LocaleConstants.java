package com.altran.kas.core.constants;

import com.altran.kas.core.enums.LocaleEnum;

public class LocaleConstants {

	public static final String LOCALE_SEPARATOR = "_";
	
	public static final String LOCALE_COMPLETE_VALIDATION = "[a-z]{2}_[A-Z]{2}$";
	
	public static final String LOCALE_DEFAULT = LocaleEnum.CA.getLabel();
	
	private LocaleConstants() {}
}
