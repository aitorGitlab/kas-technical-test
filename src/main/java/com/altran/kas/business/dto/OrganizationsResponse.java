package com.altran.kas.business.dto;

import java.util.List;

public class OrganizationsResponse {

	private String locale;
	private Boolean success;

	private OrganizationsResultDTO result;

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public OrganizationsResultDTO getResult() {
		return result;
	}

	public void setResult(OrganizationsResultDTO result) {
		this.result = result;
	}
	
	public List<OrganizationDTO> getContent() {
		return result.getResults();
	}

}
