package com.altran.kas.business.dto;

import java.util.Map;

import com.altran.kas.core.utils.LocaleUtils;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrganizationDTO {

	private String code;
	private String description;
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private String url;

	@JsonProperty("notes_translated")
	private void unpackNotesFromNestedObject(Map<String, String> notes) {
		this.description = notes.get(LocaleUtils.getLocale());
	}

	@JsonProperty("url_tornada")
	private void unpackUrlFromNestedObject(Map<String, String> urls) {
		this.url = urls.get(LocaleUtils.getLocale());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

}
