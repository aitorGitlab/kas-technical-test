package com.altran.kas.business.dto;

import java.util.List;

public class OrganizationsResultDTO {

	// Pagination - Numero total de elementos de la busqueda
	private Integer count;
	
	private List<OrganizationDTO> results;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<OrganizationDTO> getResults() {
		return results;
	}

	public void setResults(List<OrganizationDTO> results) {
		this.results = results;
	}

}
