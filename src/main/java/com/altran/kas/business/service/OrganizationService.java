package com.altran.kas.business.service;

import com.altran.kas.business.dto.OrganizationsResponse;

public interface OrganizationService {

	OrganizationsResponse getPageFromAllOrganizations(int pageNumber, int resultsPerPage);
	
	OrganizationsResponse getOrganization(String organizationCode);
	
}
