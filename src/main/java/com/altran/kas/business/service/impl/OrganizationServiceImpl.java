package com.altran.kas.business.service.impl;

import static com.altran.kas.constants.Constants.KEY_GENERATOR;
import static com.altran.kas.constants.Constants.LOCALE_PARAM;
import static com.altran.kas.constants.Constants.ORGANIZATION_CACHE;
import static com.altran.kas.constants.Constants.Q_PARAM;
import static com.altran.kas.constants.Constants.ROWS_PARAM;
import static com.altran.kas.constants.Constants.START_PARAM;

import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.altran.kas.business.dto.OrganizationsResponse;
import com.altran.kas.business.service.OrganizationService;
import com.altran.kas.core.http.HttpRequest;
import com.altran.kas.core.utils.LocaleUtils;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	private static final Logger logger = LogManager.getLogger(OrganizationServiceImpl.class);
	
	@Value("${kas-tecnical-test.ws.document-detail.rest}")
	private String documentDetailUrlRest;
	
	@Autowired
	private HttpRequest httpRequest;

	@Override
	public OrganizationsResponse getPageFromAllOrganizations(int pageNumber, int resultsPerPage) {
		logger.info("CALL - getPageFromAllOrganizations :: pageNumber={} :: resultsPerPage={}", pageNumber, resultsPerPage);
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(documentDetailUrlRest)
                .queryParam(START_PARAM, pageNumber).queryParam(ROWS_PARAM, resultsPerPage)
                .uriVariables(Collections.singletonMap(LOCALE_PARAM, LocaleUtils.getLocale()));
		return httpRequest.doGet(builder.toUriString(), OrganizationsResponse.class);
	}
	
	@Override
	@Cacheable(value = ORGANIZATION_CACHE, keyGenerator = KEY_GENERATOR)
	public OrganizationsResponse getOrganization(String organizationCode) {
		logger.info("CALL - getOrganization :: organizationCode={}", organizationCode);
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(documentDetailUrlRest)
                .queryParam(Q_PARAM, "code:"+organizationCode)
                .uriVariables(Collections.singletonMap(LOCALE_PARAM, LocaleUtils.getLocale()));
		return httpRequest.doGet(builder.toUriString(), OrganizationsResponse.class);
	}

}
