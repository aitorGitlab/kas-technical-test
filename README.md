# Kas-technical-test

[Spring Boot](http://projects.spring.io/spring-boot/) app.

## Requirements

For building and running the application you need:

- [JDK 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.altran.kas.Application` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

The application gets up at port 8090.

## API overview

The API is RESTFUL and returns results in JSON.

There are two APIs, one to obtain a list of paginated organizations and another to obtain the organizations according to a given code.

The APi are multi-language and we can indicate by means of a path parameter in which language we want the results. It can be: 'ca' (default), 'es' or 'en'.

Using the URL:

`http://localhost:8090/{locale}/organizations?page=1&size=2`

Will return the following result:

    {
       "count": 452,
       "results":    [
                {
             "code": "Code_001",
             "description": "Description example",
             "url": "https://www.website.cat"
          },
                {
             "code": "Code_002",
             "description": "Description example 2",
             "url": "https://www.website2.cat"
          }
       ]
    }

Using the URL:

`http://localhost:8090/es/organizations/{organizationCode}`

Will return the following result:

    [{
       "code": "Code_001",
       "description": "Description example",
       "url": "https://www.website.cat"
    }]


## Running the tests

The tests are in the src / test route within the package `com.altran.kas`.

They are executed from the 'OrganizationControllerMockTest' and 'OrganizationControllerTest' classes by selecting 'Run As -> JUnit test'.
